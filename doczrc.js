
import { css } from 'docz-plugin-css'

export default {
  plugins: [
    css({
      preprocessor: 'sass'
    })
  ],
  title: "Mediacurrent React Components",
  description: "A better description is needed",
}