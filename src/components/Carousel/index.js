import React from 'react';
import PropTypes from 'prop-types';
import Slider from "react-slick";

import Button from '../Button';
import Heading from '../Heading';

import './style.scss';

const Carousel = (props) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    className: "carousel__list carousel__slick"
  };
  return(
    <section className="carousel__wrapper">
      <Slider {...settings}>
        {props.items.map((item) => (
          <div className="carousel-item">
            <div className="carousel-item__media">
              <img {...item.image} />
            </div>
            <div className="carousel-item__content">
              <Heading title={item.title} level={2} classes="carousel-item__title" />
              <Button {...item.button} />
            </div>
          </div>
        ))}
      </Slider>
    </section>
  )
};

Carousel.propTypes = {
  /** Array of items holding an image tag, title, and button. */
  items: PropTypes.arrayOf(PropTypes.shape({
    /** Image info, with src and alt. */
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string
    }),
    /** Title */
    title: PropTypes.string,
    /** Button */
    button: PropTypes.shape({
      /** Custom click handler function. */
      onClick: PropTypes.func,
      /** When populated with a url, this component renders a <a> vs a <button> */
      href: PropTypes.string,
      /** The text which renders in the standard browser tooltip on hover */
      info: PropTypes.string,
      /** Button or link text */
      text: PropTypes.string,
      /** Extra classes */
      classes: PropTypes.string
    })
  })),

}

export default Carousel;
