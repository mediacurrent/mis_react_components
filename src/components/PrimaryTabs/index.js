import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

const PrimaryTabs = ({items}) => (
  <div className="primary-tabs">
    <ul className="primary-tabs__items">
      {items.map((item, key) => {
        const classes = classNames(
          "primary-tabs__link",
          {[`is-active`]: item.active}
        );
        return(
          <li className="primary-tabs__item" key={key}>
            <a href={item.url} className={classes}>
              {item.text}
            </a>
          </li>
        );
      })}
    </ul>
  </div>
);

PrimaryTabs.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string,
    text: PropTypes.string,
    active: PropTypes.bool
  }))
}

export default PrimaryTabs;
