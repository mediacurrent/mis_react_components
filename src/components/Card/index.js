import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Body from '../Body';
import Eyebrow from '../Eyebrow';
import Heading from '../Heading';
import Media from '../Media';

import './style.scss';

const Card = (props) => {

  const classes = classNames(
    'card',
    {[`${props.classes}`]: props.classes}
  )
  return(
    <article className={classes}>
      {props.media && (
        <div className="card__media">
          <Media {...props.media} />
        </div>
      )}
      <div className="card__content">
        {props.eyebrow && <Eyebrow {...props.eyebrow} />}
        {props.heading && <Heading {...props.heading} />}
        {props.subhead && <Heading {...props.subhead} />}
        {props.text && <Body text={props.text} />}
        {props.link && (
          <a href={props.link.url} className="card__link">
            {props.link.text}
          </a>
        )}
      </div>
    </article>
  )
}

Card.propTypes = {
  text: PropTypes.string,
  /** Eyebrow Component Properties. */
  eyebrow: PropTypes.shape({
    text: PropTypes.string,
    classes: PropTypes.string
  }),
  /** Heading Component Properties. */
  heading: PropTypes.shape({
    level: PropTypes.string,
    classes: PropTypes.string,
    title: PropTypes.string,
    url: PropTypes.string
  }),
  /** Heading Component Properties */
  subheading: PropTypes.shape({
    level: PropTypes.string,
    classes: PropTypes.string,
    title: PropTypes.string,
    url: PropTypes.string
  }),
  link: PropTypes.shape({
    url: PropTypes.string,
    text: PropTypes.string
  }),
  /** Media Component Properties. */
  media: PropTypes.shape({
    /** Image Tag */
    image: PropTypes.symbol,
    /** Video Iframe */
    video: PropTypes.symbol,
    /** Credit / Caption */
    caption: PropTypes.string
  }),
  /** Additional classes. */
  classes: PropTypes.string
}

export default Card;