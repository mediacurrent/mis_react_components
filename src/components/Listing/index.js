import React from 'react';

const Listing = (props) => {

  return(
    <ul className="listing">
      {props.items.map((item, key) => {
        return(
          <li className="listing-item" key={key}>
            <div className="listing-item__media">
              {item.image}
            </div>
            <div className="listing-item__content">
              {item.heading && <Heading {...item.heading} />}
              {item.body && <Body text={item.body} />}
              {item.link && (
                <a className="listing-item__link" href={item.link.url}>
                  {item.link.text }
                </a>
              )}
            </div>
          </li>
        );
      })}
    </ul>
  );
}

export default Listing;
