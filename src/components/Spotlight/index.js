import React from 'react';
import PropTypes from 'prop-types';

import Body from '../Body';
import Eyebrow from '../Eyebrow';
import Heading from '../Heading';

const Spotlight = (props) => {
  return(
    <section className="spotlight">
      <Heading title={props.title} classes="spotlight__section-title" />
      <div className="spotlight__group">
        {props.items.map((item, key) => (
          <article className="spotlight__item" key={key}>
            {item.image && (
              <div
                className="spotlight__image"
                dangerouslySetInnerHTML={{__html: item.image}}
              />
            )}
            {item.eyebrow && <Eyebrow text={item.eyebrow} classes="spotlight__eyebrow" />}
            <Heading title={item.title} level="3" classes="spotlight__title" />
            {item.subtitle && <Heading title={item.subtitle} level="4" classes="spotlight__subhead"/>}
            {item.text && <Body text={item.text} />}
            {item.link && <a href={item.link.url } className="spotlight__link">{item.link.text}</a>}
          </article>
        ))}
      </div>
    </section>
  )
}

Spotlight.propTypes = {
  /** Main title. */
  title: PropTypes.string.isRequired,
  /** Items listing. */
  items: PropTypes.arrayOf(PropTypes.shape({
    image: PropTypes.string,
    eyebrow: PropTypes.string,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    link: PropTypes.shape({
      url: PropTypes.string,
      text: PropTypes.string
    })
  }))
}

export default Spotlight;
