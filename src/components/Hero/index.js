import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Body from '../Body';
import Button from '../Button';
import Eyebrow from '../Eyebrow';
import Heading from '../Heading';
import Media from '../Media';

import './style.scss';

const Hero = (props) => {
  const classes = classNames(
    "hero",
    {[`${props.classes}`]: props.classes}
  )
  return(
    <section className={classes}>
      {props.media && <Media {...props.media} />}
      <div className="hero__content">
        {props.eyebrow && <Eyebrow text={props.eyebrow} classes="hero__eyebrow" />}
        {props.heading && <Heading {...props.heading} />}
        {props.body && <Body classes="hero__body" text={props.body}/>}
        {props.button && <Button {...props.button} />}
      </div>
    </section>
  );
}

Hero.propTypes = {
  /** Hero Properties with image or video */
  media: PropTypes.object,
  /** Eyebrow String */
  eyebrow: PropTypes.string,
  /** Heading Component Properties */
  heading: PropTypes.object,
  /** Body String */
  body: PropTypes.string,
  /** Button Component Properties */
  button: PropTypes.object,
  /** Extra classes */
  classes: PropTypes.string
}


export default Hero;
