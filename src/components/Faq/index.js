import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Faq = (props) => (
  <section className="faq">
    {props.items.map(item => {
      return(
        <article className="faq-item">
          <h2 className="faq-item__q">{item.question}</h2>
          <p className="faq-item__a">{item.answer}</p>
          {item.link && <a href={item.link.href} class="faq-item__link">{item.link.text}</a>}
        </article>
      )
    })}
  </section>
)

Faq.propTypes = {
  /** Array of items. */
  items: PropTypes.arrayOf(PropTypes.shape({
    question: PropTypes.string.isRequired,
    answer: PropTypes.string.isRequired,
    link: PropTypes.shape({
      href: PropTypes.string,
      text: PropTypes.string
    }).isRequired
  }))
}

export default Faq;
