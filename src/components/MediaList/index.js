import React from 'react';

const MediaList = ({items}) => (
  <section className="media-list">
    {items.map((item, key) => {
      <Media {...item} />
    })}
  </section>
);

export default MediaList;
