import React from 'react';

const SampleList = (props) => {

  return(
    <section class="sample-list">
      {props.heading && <h2 class="sample-list__heading">{props.heading}</h2>}
      {props.items.map((item, key) => (
        <article class="sample-list__item" key={key}>
          <h3>{item.title}</h3>
          <p>{item.desc}</p>
        </article>
      ))}
    </section>
  )
}

export default SampleList;
