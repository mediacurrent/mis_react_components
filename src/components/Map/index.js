import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Map = ({map, address}) => {

  return(
    <>
      {address && (
        <section className="map map--2col">
          <div className="map__wrapper">
            <div
              className="map__embed"
              dangerouslySetInnerHTML={{__html:map}}
            />
          </div>
          <div className="map__text">
            <h2 className="map__location">{address.location}</h2>
            <p className="map__address">
              {address.street}
              <br />
              {address.city_state}
            </p>
          </div>
        </section>
      )}
      {!address && (
        <section className="map map--full">
          <div
            className="map__embed"
            dangerouslySetInnerHTML={{__html:map}}
          />
        </section>
      )}
    </>
  )
}

Map.propTypes = {
  map: PropTypes.string.isRequired,
  address: PropTypes.shape({
    location: PropTypes.string,
    street: PropTypes.string,
    city_state: PropTypes.string
  })
}

export default Map;
