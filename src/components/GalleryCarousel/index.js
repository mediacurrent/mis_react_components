import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

import Heading from '../Heading';
import Body from '../Body';

import 'slick-carousel/slick/slick.scss';
import './style.scss';

const GalleryCarousel = (props) => {
  const settings = {
    customPaging: (i) => {
      return(
        <a className="gallery-carousel__nav-item">
          <img {...props.items[i].thumb} />
        </a>
      )
    },
    className: "gallery-carousel",
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  }
  return(
    <>
      {props.title && <Heading {...{
        title: props.title,
        level: props.title_level || 2,
        classes: "gallery-carousel__title"
      }} />}
      {props.intro && <Body classes="gallery-carousel__intro-text" text={props.intro}/>}
      <Slider {...settings}>
        {props.items.map(item => {
          return(
            <div class="gallery-carousel-item">
              <img src={item.src} className="gallery-carousel-item__media" alt={item.alt } />
              <p className="gallery-carousel-item__text">{item.alt}</p>
            </div>
          )
        })}
      </Slider>
    </>
  );
}

GalleryCarousel.propTypes = {
  /** Optional Title */
  title: PropTypes.string,
  /** Optional Title level. Defaults to 2 */
  title_level: PropTypes.number,
  /** Optional Intro */
  intro: PropTypes.string,
  /** Array of items [{src, alt, thumb:[{src, alt}]}] */
  items: PropTypes.arrayOf(PropTypes.shape({
    src: PropTypes.string,
    alt: PropTypes.string,
    thumb: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string
    })
  }))
}

export default GalleryCarousel;
