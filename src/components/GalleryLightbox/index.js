import React from 'react';
import PropTypes from 'prop-types';
import Lightbox from 'lightbox-react';

import Body from '../Body';
import Heading from '../Heading';

// @todo: figure out why this doesn't work or the stock styles don't work.
// import 'lightbox-react/style.css';
import './style.scss';

class GalleryLightbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photoIndex: 0,
      isOpen: false
    }
  }

  render() {
    const { title, intro, images } = this.props;
    const { photoIndex, isOpen } = this.state;
    return (
      <>
        {title && <Heading title={title} classes="gallery-lightbox__title" />}
        {intro && <Body classes="gallery-lightbox__intro-text" body={intro} />}
        <Lightbox
          mainSrc={images[photoIndex].src}
          imageTitle={images[photoIndex].title || null}
          imageCaption={images[photoIndex].caption || null}
          nextSrc={images[(photoIndex + 1) % images.length].src}
          prevSrc={images[(photoIndex + images.length - 1) % images.length].src}
          onCloseRequest={() => this.setState({ isOpen: false })}
          onMovePrevRequest={() =>
            this.setState({
              photoIndex: (photoIndex + images.length - 1) % images.length,
            })
          }
          onMoveNextRequest={() =>
            this.setState({
              photoIndex: (photoIndex + 1) % images.length,
            })
          }
        />
        <button onClick={() => this.setState({ isOpen: true })}>
          <img src={images[0].thumb} alt="Click to Open" />
        </button>
      </>
    );
  }
};

GalleryLightbox.propTypes = {
  title: PropTypes.string,
  intro: PropTypes.string,
  images: PropTypes.arrayOf(PropTypes.shape({
    src: PropTypes.string,
    title: PropTypes.string,
    caption: PropTypes.string,
    thumb: PropTypes.string
  }))
}

export default GalleryLightbox;
