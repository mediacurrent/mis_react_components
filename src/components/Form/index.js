import React from 'react';
import PropTypes from 'prop-types';
import { slugify } from 'transliteration';

import Button from '../Button';

import './style.scss';

class Form extends React.Component {

  componentDidMount() {
    const formItems = document.getElementsByClassName('.js-form-item');
    console.log(formItems);
    /* formItems.map((item) => {
      // Check if value is filled out and float if not.
      floatLabel(item);
      // Attach action on focus and blur.
      item.on('focus, blur', (element) => floatLabel(element));
    }) */
  }
  // If the input contains any value, add the class. If not, remove it.
  floatLabel = (element) => {
    if (element.val() !== '') {
      element.parent().addClass('floated');
    }
    else {
      element.parent().removeClass('floated');
    }
  }
  render() {
    return(
      <form>
        {this.props.items.map((item, key) => {
          const id = slugify(item.label);
          return(
            <div className={`form-item form-item--${item.type}`} key={key}>
              <input
                id={id}
                type={item.type}
                className="form-item__input js-form-item"
                aria-describedby="required-message"
                required
              />
              <label htmlFor={id} className="form-item__label required-field">
                {item.label}
              </label>
              <div id="required-message" className="visually-hidden">This field is required.</div>
            </div>
          )
        })}
        <div className="form-item form-item--textarea">
          <textarea className="form-item__input js-form-item" id="message" name="messagetext"></textarea>
          <label htmlFor="message" className="form-item__label required-field">Message</label>
        </div>
        <Button {...this.props.button} />
      </form>
    );
  }
}

Form.propTypes = {
  /** Form Items. */
  items: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.oneOf('text', 'email'),
    label: PropTypes.string,
    required: PropTypes.bool
  })),
  /** Button */
  button: PropTypes.object
}

export default Form;
