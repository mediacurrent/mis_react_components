import React from 'react';

import {Body, Eyebrow, Heading} from '../../index';

const Promo = (props) => {
  const classes = classNames({
    promo: true,
    [`promo${props.classes}`]: props.classes
  });
  return(
    <article className={classes}>
      {props.image && (
        <div className="promo__media">
          {props.image}
        </div>
      )}
      <div className="promo__content">
        <header>
          {props.eyebrow && <Eyebrow {...props.eyebrow} />}
          <Heading title={props.title} classes="promo__headline" />
          {props.subtitle && <Heading level="3" title={props.subtitle} classes="promo__subhead" />}
        </header>
        <body>
          <Body text={props.body} />
        </body>
        <footer>
          {props.link && <a className="promo__link" href={props.link.url}>{props.link.text}</a>}
        </footer>
      </div>
    </article>
  )
}

export default Promo;
