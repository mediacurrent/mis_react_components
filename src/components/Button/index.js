import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

const Button = (button) => {
  const href = button.href;
  const info = button.info || button.text;
  const buttonClasses = classNames({
    button: true,
    [`button--${button.classes}`]: button.classes
  });
  const Element = href ? 'a' : 'button';
  const onClickCallback = (e) => {
    if (typeof button.onClick === 'function') {
      e.preventDefault();
      button.onClick(e);
    }
  };

  return(
    <Element
      className={buttonClasses}
      href={href}
      title={info}
      aria-label={info}
      onClick={(e) => onClickCallback(e)}
    >
      {button.text}
    </Element>
  );
};

Button.propTypes = {
  /** Custom click handler function. */
  onClick: PropTypes.func,
  /** When populated with a url, this component renders a <a> vs a <button> */
  href: PropTypes.string,
  /** The text which renders in the standard browser tooltip on hover */
  info: PropTypes.string,
  /** Button or link text */
  text: PropTypes.string,
  /** Extra classes. Note: the prefix of `button--` will be appended. */
  classes: PropTypes.string
};

export default Button;
