# Mediacurrent React Component Library

Welcome to the React export of the Twig component Library.
https://bitbucket.org/mediacurrent/mis_twig_components

There's a lot to do here, so get started!

## Usage

TBD

## Developing

The project uses Docz to view how the items are to be used.

* `nvm use`: Will get your system on the same version of Node.
* `npm install`: Install all dependencies.
* `npm run start`: This will run the demo Docz server on http://127.0.0.1:3000
* Develop and submit PR's!